package com.qa.HibernateAccess.ManyToManyExample;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;



/**
 * @author Kat
 *
 */
@Entity
public class Company {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	private String name;
	
	
	@ManyToMany(mappedBy="listOfCompanies")
	private List<Person> listOfPeople;

	public Company(int id, String name, ArrayList<Person> empList) {
		super();
		this.id = id;
		this.name = name;
		this.listOfPeople = empList;
	}
	
	
	public Company(int id, String name) {
		super();
		this.id = id;
		this.name = name;
		this.listOfPeople = new ArrayList<Person>();
	}
	
	public Company(){}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public List<Person> getEmpList() {
		return listOfPeople;
	}


	public void setEmpList(ArrayList<Person> empList) {
		this.listOfPeople = empList;
	}
	
	
	
}
