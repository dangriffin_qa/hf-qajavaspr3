package com.qa.HibernateAccess.Annotated;

import java.util.ArrayList;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class Main {
	public static void main(String[] args) {
		new Main();
	}

	//Still relies on using hibernate.cfg.xml
	public Main() {

		Configuration cfg = new Configuration().configure();

		// set up the service registry
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder()
				.applySettings(cfg.getProperties()).buildServiceRegistry();

		// set up the session factory
		SessionFactory sessionFactory = cfg
				.addAnnotatedClass(Animal.class)
				.buildSessionFactory(serviceRegistry);

		

		Session session = sessionFactory.openSession();

		Query query = session.createQuery("FROM Animal");
		ArrayList<Animal> results = new ArrayList<Animal>(query.list());

		session.close();

		for (Animal a : results) {
			System.out.println(a);
		}

	}
}
