package com.qa.HibernateAccess.ManyToManyExample;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class Main {

	public static void main(String[] args) {
		// setup the database

		Configuration cfg = new Configuration().configure();

		// set up the service registry
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder()
				.applySettings(cfg.getProperties()).buildServiceRegistry();

		// set up the session factory
		SessionFactory sessionFactory = cfg.addAnnotatedClass(Person.class)
				.addAnnotatedClass(Company.class)
				.buildSessionFactory(serviceRegistry);

		// create the objects
		Person alice = new Person(0, "Alice");
		Person bob = new Person(0, "Bob");
		Person mallory = new Person(0, "Mallory");
		Person eve = new Person(0, "Eve");

		Company a = new Company(0, "A");
		Company b = new Company(0, "B");
		Company c = new Company(0, "C");

		ArrayList<Person> pair1 = new ArrayList<Person>();
		pair1.add(alice);
		pair1.add(bob);
		pair1.add(eve);

		ArrayList<Person> pair2 = new ArrayList<Person>();
		pair2.add(mallory);
		pair2.add(eve);

		ArrayList<Person> pair3 = new ArrayList<Person>();
		pair3.add(alice);
		pair3.add(bob);
		pair3.add(mallory);
		pair3.add(eve);

		ArrayList<Company> aliceAndBobList = new ArrayList<Company>();
		aliceAndBobList.add(a);
		aliceAndBobList.add(c);

		ArrayList<Company> malloryList = new ArrayList<Company>();
		malloryList.add(b);
		malloryList.add(c);

		ArrayList<Company> eveList = new ArrayList<Company>();
		eveList.add(a);
		eveList.add(b);

		a.setEmpList(pair1);
		b.setEmpList(pair2);
		c.setEmpList(pair3);

		alice.setEmpList(aliceAndBobList);
		bob.setEmpList(aliceAndBobList);
		mallory.setEmpList(malloryList);
		eve.setEmpList(eveList);

		// save to the database
		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		session.saveOrUpdate(a);
		session.saveOrUpdate(b);
		session.saveOrUpdate(c);
		session.saveOrUpdate(alice);
		session.saveOrUpdate(bob);
		session.saveOrUpdate(mallory);
		session.saveOrUpdate(eve);

		tx.commit();

		// get from the database

		session.close();

		session = sessionFactory.openSession();
		
		Query qry = session.createQuery("from Person");

		ArrayList<Person> results = new ArrayList<Person>(qry.list());

				session.close();


		for (Person e : results) {
			System.out.println(e);
		}
	}

}
