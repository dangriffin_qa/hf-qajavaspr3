package com.qa.HibernateAccess;

import java.util.Date;


public class Animal {

	private int animalID;
	
	private String name;
	private String type;
	private Date dob;

	
	public Animal() {
	}

	public Animal(int animalID, String name,
			String type, Date dob) {
		this.animalID = animalID;
		this.name = name;
		this.type = type;
		this.dob = dob;
	}

	public int getAnimalID() {
		return animalID;
	}

	public void setAnimalID(int animalID) {
		this.animalID = animalID;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	@Override
	public String toString(){
		return "Animal " + animalID + ": " + name + ", " + type + ", " + dob;
	}
}
