package com.qa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ParameterPassingController {
	
	@RequestMapping("/")
	@ResponseBody
	public String welcome() {
		return "ParameterPassing Example is running: try /sayhi, /greet/{name} ...";
	}
	
	@RequestMapping("/sayhi")
	@ResponseBody
	public String usingRequestParam(@RequestParam(value = "name", 
												  required = false,
												  defaultValue = "World") String name) {
		return "(using query parameter) Hello " + name;
	}
	
	@RequestMapping("/greet/{name}")
	@ResponseBody
	public String usingPathParam(@PathVariable("name") String name) {
		return "(using path) Hi " + name;
	}
	
	@RequestMapping("/to/{name}/say/{greeting}")
	@ResponseBody
	public String usingPathParamWithMultipleVariables(@PathVariable("name") String name,
													  @PathVariable("greeting") String greeting) {
		return "(using path) " + greeting+ " " + name;
	}
	
}


