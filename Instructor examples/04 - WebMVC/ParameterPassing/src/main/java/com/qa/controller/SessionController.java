package com.qa.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("sessionValue")
public class SessionController {
	
	@RequestMapping("/session/get")
	@ResponseBody
	public String obtainFromSession(Model model) {
		return "(using session) sessionValue is currently " + model.asMap().get("sessionValue");
	}
	
	@RequestMapping("/session/set/{value}")
	@ResponseBody
	public String setIntoSession(@PathVariable("value") String value, Model model, HttpSession dummy) {
		model.addAttribute("sessionValue", value);
		return "(using session) Changing sessionValue to " + value;
	}
	
}


