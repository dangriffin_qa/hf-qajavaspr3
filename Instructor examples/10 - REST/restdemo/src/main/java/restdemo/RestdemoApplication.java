package restdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class RestdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestdemoApplication.class, args);
    }

    @RequestMapping("/")
    public Animal home(){
    	return new Animal("Angus", "Rabbit", 2, "black");
    }
    
    @RequestMapping("/list")
    public Animal[] list(){
    	Animal[] arr = {new Animal("Angus", "Rabbit", 2, "black"), new Animal("Honey", "Dog", 1, "Ginger")}; 
    	return arr;
    }
    
    @RequestMapping(value="/receive", method = RequestMethod.POST)
    public String recieveAnimal(@RequestBody Animal a) {
    	System.out.println(a);
    	return "I recieved this animal: " + a;
    }
}
