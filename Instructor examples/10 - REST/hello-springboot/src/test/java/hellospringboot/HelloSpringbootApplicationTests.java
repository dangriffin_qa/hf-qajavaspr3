package hellospringboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.qa.hellospringboot.HelloSpringbootApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HelloSpringbootApplication.class)
public class HelloSpringbootApplicationTests {

	@Test
	public void contextLoads() {
	}

}
