package com.qa.hellospringboot;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@ComponentScan("com.qa")
public class HelloSpringbootApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(HelloSpringbootApplication.class, args);

		System.out.println("Beans provided by SpringBoot:");

		String[] beans = ctx.getBeanDefinitionNames();
		Arrays.sort(beans);
		for (String name : beans) {
			System.out.println(name);
		}
	}

}
