package com.qa.SpringForms.Aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public 	class PersonAspect {
	@Around("execution(* com.qa.SpringForms.beans.*.get*(..))")
	public Object log(ProceedingJoinPoint pre) throws Throwable {
		System.out.println("GET method being called on a bean " + pre.getSignature().getDeclaringTypeName() + " method " + pre.getSignature().getName());
		Object o = pre.proceed();
		//System.out.println("GET method, after it has run.");
		return o;
	}
	
	@Around("execution(public void com.qa.SpringForms.beans.Person.set*(..))")
	public void logSet(ProceedingJoinPoint pre) throws Throwable {
		System.out.println("SET method being called on a bean " + pre.getSignature().getDeclaringTypeName() + " method " + pre.getSignature().getName());
//		System.out.println("Nope, not doing that");
		pre.proceed();
	}
	
	@Pointcut("execution(* com.qa.SpringForms.HomeController.*(..))")
	public void pointCut(){}
	
	@Before("pointCut()")
	public void saySomething(){
		System.out.println("Before the homecontroller does anything... ");
	}
	
	@After("pointCut()")
	public void saySomethingElse(){
		System.out.println("Run after the homecontroller is doing something");
	}
	


}
