package com.qa;

import java.util.HashMap;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

	public static void main(String[] args) {
		new App();
	}

	public App() {

		// load the application context
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

		// get the beans
		System.out.println("*** getting each bean ***");

		Message msg1 = (Message) context.getBean("message1");
		Message msg2 = (Message) context.getBean("message2");

		// print out the message
		System.out.println(msg1.getMessage());
		System.out.println(msg2.getMessage());

		// we can also get all the beans of one type easily
		System.out.println("*** Using a map ***");

		HashMap<String, Message> list = new HashMap<String, Message>(
				context.getBeansOfType(Message.class));
		for (String key : list.keySet()) {
			System.out.println(list.get(key).getMessage());
		}
	}

}
