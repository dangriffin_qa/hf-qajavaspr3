package com.qa;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfig {
	
	@Bean
	public Message message1(){
		Message m = new Message();
		m.setMessage("Using the setter");
		return m;
	}
	
	@Bean Message message2(){
		return new Message("This used the constructor");
	}

}
