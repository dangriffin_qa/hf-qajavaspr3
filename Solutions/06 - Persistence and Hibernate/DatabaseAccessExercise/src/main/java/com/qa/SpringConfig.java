package com.qa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import com.qa.beans.Animal;
import com.qa.beans.Owner;

@Configuration
public class SpringConfig {
	
	//Bean for the database connection
	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		//properties for hibernate
		Properties prop = new Properties();
		prop.setProperty("hibernate.hbm2ddl.auto", "update");
		prop.setProperty("hibernate.dialect","org.hibernate.dialect.HSQLDialect");
		prop.setProperty("hibernate.globally_quoted_identifiers", "true");
		prop.setProperty("show_sql", "true");
		
		//create a local session factory bean, scan the com.qa.beans 
		//package for any annotated beans
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan(new String[] {"com.qa.beans"});
		sessionFactory.setHibernateProperties(prop);
		return sessionFactory;
	}

	@Bean
	public DataSource dataSource() {
		BasicDataSource db = new BasicDataSource();
	    db.setDriverClassName("org.hsqldb.jdbcDriver");
	    db.setUrl("jdbc:hsqldb:file:c:/db/vet");
	    db.setUsername("SA");
	    db.setPassword("");
		return db;
		
		//Use if you want an SQL Database
//		BasicDataSource dataSource = new BasicDataSource();
//		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
//		dataSource.setUrl("jdbc:mysql://localhost/vet");
//		dataSource.setUsername("root");
//		dataSource.setPassword("root");
//		return dataSource;
	}
	
	
	private Date getDate(String dateStr){
		try {
			return new SimpleDateFormat("dd-MM-yyyy").parse(dateStr);
		} catch (ParseException e) {
			return new Date();
		}
	}
	
	//beans for five animals
	@Bean
	public Animal alpha(){
		return new Animal(0, "Alpha", "Dog", getDate("10-01-2014"));//, alice());
	}
	
	@Bean
	public Animal beta(){
		return new Animal(1, "Beta", "Dog", getDate("15-01-2014"));//, alice());
	}
	
	@Bean
	public Animal gamma(){
		return new Animal(2, "Gamma", "Cat", getDate("08-03-2008"));//, bob());
	}
	
	@Bean
	public Animal delta(){
		return new Animal(3, "Delta", "Cat", getDate("09-01-2009"));//, bob());
	}
	
	@Bean
	public Animal epsilon(){
		return new Animal(4, "Epsilon", "Rabbit", getDate("20-03-2013"));//, bob());
	}
	
	//beans for an owner
	@Bean 
	public Owner alice(){
		Owner a = new Owner(0, "Alice", "Alice@email.com");
		ArrayList<Animal> list = new ArrayList<Animal>();
		list.add(alpha());
		list.add(beta());
		a.setAnimals(list);
		return a;
	}
	
	@Bean 
	public Owner bob(){
		Owner a = new Owner(0, "Bob", "bob@email.com");
		ArrayList<Animal> list = new ArrayList<Animal>();
		list.add(gamma());
		list.add(delta());
		list.add(epsilon());
		a.setAnimals(list);
		return a;
	}
		
}
