package com.qa;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.qa.beans.Message;
import com.qa.beans.Owner;
import com.qa.beans.Person;

@Configuration
@PropertySource("classpath:my.properties")
public class SpringConfig {
	
	//setup the message beans
	@Bean
	public Message msg1(){
		Message msg = new Message();
		msg.setMessage("This is message 1");
		return msg;
	}
	
	@Bean
	public Message msg2(){
		Message msg = new Message();
		msg.setMessage("This is message 2");
		return msg;
	}
	
	@Bean
	public Message msg3(){
		Message msg = new Message();
		msg.setMessage("This is message 3");
		return msg;
	}
	
	
	//set up the owner bean, we use msg1() to get the message bean rather than
	//create a new message in here
	@Bean 
	public Owner ownerBean(){
		Owner o = new Owner();
		o.setName("Kat");
		ArrayList<Message> list = new ArrayList<Message>();
		list.add(msg1());
		list.add(msg2());
		list.add(msg3());
		o.setList(list);
		return o;
	}
	
	
	//needed to load in the properties file
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}


	@Bean
	public Person kat(){
		//the values are automatically passed into the person using the placeholder!
		return new Person();
	}

}
