package com.qa.beans;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

public class AutowiredBean implements InitializingBean, DisposableBean{
	
	public AutowiredBean() {}
	
//	***** Autowiring by variable ****** 
//	@Autowired
//	private String name;
//	@Autowired
//	private int ID;
	
	//***** Autowiring by variable ******	
	private String name;
	private int ID;
	
	@Autowired
	public void setName(String name){ this.name = name;}
	@Autowired
	public void setID(int ID){this.ID = ID;}
	
//	//***** Autowired by constructor *****
//	private String name;
//	private int ID;
//	
//	public AutowiredBean(String name, int ID){
//		this.name = name;
//		this.ID = ID;
//	}
	
	
	@Override
	public String toString(){
		return ID + ": " + name;
	}

	@Override
	public void destroy() throws Exception {
		System.out.println("Destroying!");
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("After Properties have been set the bean looks like: " + this.toString());
	}

}
