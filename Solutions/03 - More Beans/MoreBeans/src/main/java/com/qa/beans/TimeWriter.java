package com.qa.beans;

import java.lang.reflect.Method;
import java.util.Date;

import org.springframework.beans.factory.support.MethodReplacer;

public class TimeWriter implements MethodReplacer {

	@Override
	public Object reimplement(Object obj, Method method, Object[] args) throws Throwable {
		
		//get the time
		String time = new Date(System.currentTimeMillis()).toString();
		
		//print out the time and the message
		System.out.println(time + ": " + args[0]);
		
		//return null as the original method had no return type so we don't need to worry about it
		return null;
	}

}
