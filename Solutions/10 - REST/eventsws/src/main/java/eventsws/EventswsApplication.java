package eventsws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import eventsws.model.EventsDAO;
import eventsws.model.EventsDAOImpl;

@SpringBootApplication
public class EventswsApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventswsApplication.class, args);
    }
    
    @Bean
	public EventsDAO dao() {
		return new EventsDAOImpl();
	}
}
