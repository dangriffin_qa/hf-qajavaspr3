package com.qa.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.qa.InsufficientBalanceException;

@Entity
public class BankAccount {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int ID;
	private double balance;
	
	public BankAccount(int ID, double balance){
		this.ID = ID;
		this.balance = balance;
	}
	
	public BankAccount(){}
	
	public int getID(){
		return ID;
	}
	
	public void setID(int newID){
		this.ID = newID;
	}
	
	public double getBalance(){
		return balance;
	}
	
	public void increment(double amount){
		balance = balance + amount;
	}
	
	public void decrement(double amount) throws InsufficientBalanceException {
		if (balance > amount){
			balance = balance - amount;
		} else {
			throw new InsufficientBalanceException(amount, balance);
		}
	}
	
	@Override
	public String toString(){
		return ID + ": " + balance;
	}
}
