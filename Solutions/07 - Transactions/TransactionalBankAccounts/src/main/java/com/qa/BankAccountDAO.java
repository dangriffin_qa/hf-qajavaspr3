package com.qa;

import java.util.ArrayList;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.qa.beans.BankAccount;

@Transactional
public class BankAccountDAO {
	@Autowired
	SessionFactory sessionFactory;

	/*
	 * Simple helper method to create some beans and save them to the database.
	 * Call this to initialise everything
	 */
	public void createBeansAndSave() {
		Session openSession = sessionFactory.openSession();

		BankAccount a = new BankAccount(0, 100);
		BankAccount b = new BankAccount(0, 1000);

		Transaction tx = openSession.beginTransaction();
		openSession.save(a);
		openSession.save(b);

		openSession.flush();
		tx.commit();
		openSession.close();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<BankAccount> getAllAccounts() {

		Session sess = sessionFactory.openSession();
		Query query = sess.createQuery("from bankaccount");
		ArrayList<BankAccount> results = new ArrayList<BankAccount>(query.list());

		return results;
	}

	public BankAccount getAccountByID(int id) {
		Session sess = sessionFactory.openSession();

		String hql = "from BankAccount where ID=:id";
		Query query = sess.createQuery(hql);
		query.setInteger("id", id);
		BankAccount account = (BankAccount) query.list().get(0);

		return account;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class )
	public void transferFunds(BankAccount fromAccount, BankAccount toAccount, final Double transferAmount) throws InsufficientBalanceException{
		Session session = sessionFactory.getCurrentSession();

		System.out.println("Transfering");
		toAccount.increment(transferAmount);
		System.out.println("Done to " + toAccount.getBalance());
		session.merge(toAccount);
		System.out.println("saved");

		fromAccount.decrement(transferAmount);
		System.out.println("Done from" + fromAccount.getBalance());
		session.merge(fromAccount);
		System.out.println("Saved");

	}


}
