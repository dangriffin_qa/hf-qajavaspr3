package com.qa;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.qa.beans.BankAccount;

public class Main {
	public static void main(String[] args) {

			new Main(false);

	}

	public Main(boolean createDB) {
		// get the context
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				SpringConfig.class);
		
		BankAccountDAO dao = context.getBean(BankAccountDAO.class);

		if (createDB)
			dao.createBeansAndSave();

		BankAccount a = dao.getAccountByID(17);
		BankAccount b = dao.getAccountByID(18);
		// print out the values in the account
		System.out.println("*** BEFORE ***");
		System.out.println(a);
		System.out.println(b);

		// try to move money from one account to the other
		try {
			dao.transferFunds(a, b, 50.0);
		} catch (InsufficientBalanceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

				// get the bank account beans from the database
		a = dao.getAccountByID(17);
		b = dao.getAccountByID(18);

		System.out.println("*** AFTER ***");
		System.out.println(a);
		System.out.println(b);
	}
	

}
