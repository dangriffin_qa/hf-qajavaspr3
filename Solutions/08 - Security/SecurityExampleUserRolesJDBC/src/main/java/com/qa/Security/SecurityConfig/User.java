package com.qa.Security.SecurityConfig;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {

	@Id
	int id;
	
	String name;
	String password;
	Boolean enabled;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	
}
