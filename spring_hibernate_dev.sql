-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 05, 2015 at 01:28 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `spring_hibernate_dev`
--
CREATE DATABASE IF NOT EXISTS `spring_hibernate_dev` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `spring_hibernate_dev`;

-- --------------------------------------------------------

--
-- Table structure for table `animal`
--

CREATE TABLE IF NOT EXISTS `animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `ownerID` int(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `pie` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ownerID` (`ownerID`),
  KEY `FK_a2ehcjaad4agblm4my5t0v38b` (`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `animal`
--

INSERT INTO `animal` (`id`, `name`, `type`, `dob`, `ownerID`, `owner_id`, `pie`) VALUES
(27, 'Honey', 'Dog', '2014-09-19', 3, NULL, NULL),
(28, 'Midas', 'Cat', '2014-09-19', 3, NULL, NULL),
(35, 'Bob', 'Fish', '2014-03-20', 1, NULL, NULL),
(36, 'Swimmy', 'Hamster', '2014-08-15', 1, NULL, NULL),
(37, 'Bubble', 'Hamster', '1982-09-28', 3, NULL, NULL),
(38, 'Angus2', 'Bunny', '0036-06-01', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `name`) VALUES
(4, 'A'),
(5, 'B'),
(6, 'C');

-- --------------------------------------------------------

--
-- Table structure for table `link`
--

CREATE TABLE IF NOT EXISTS `link` (
  `listOfPeople_id` int(11) NOT NULL,
  `listOfCompanies_id` int(11) NOT NULL,
  KEY `FK_c993421e3a204396ae40ea15f42` (`listOfCompanies_id`),
  KEY `FK_49d47e2077244b2fa0158cf6377` (`listOfPeople_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `link`
--

INSERT INTO `link` (`listOfPeople_id`, `listOfCompanies_id`) VALUES
(5, 4),
(5, 6),
(6, 4),
(6, 6),
(7, 5),
(7, 6),
(8, 4),
(8, 5);

-- --------------------------------------------------------

--
-- Table structure for table `owner`
--

CREATE TABLE IF NOT EXISTS `owner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `owner`
--

INSERT INTO `owner` (`id`, `name`, `email`) VALUES
(1, 'Barry', 'Barry@email.com'),
(3, 'Kat', 'kat@email.com');

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `name`) VALUES
(5, 'Alice'),
(6, 'Bob'),
(7, 'Mallory'),
(8, 'Eve');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `animal`
--
ALTER TABLE `animal`
  ADD CONSTRAINT `animal_ibfk_1` FOREIGN KEY (`ownerID`) REFERENCES `owner` (`id`),
  ADD CONSTRAINT `FK_a2ehcjaad4agblm4my5t0v38b` FOREIGN KEY (`owner_id`) REFERENCES `owner` (`id`);

--
-- Constraints for table `link`
--
ALTER TABLE `link`
  ADD CONSTRAINT `FK_49d47e2077244b2fa0158cf6377` FOREIGN KEY (`listOfPeople_id`) REFERENCES `person` (`id`),
  ADD CONSTRAINT `FK_c993421e3a204396ae40ea15f42` FOREIGN KEY (`listOfCompanies_id`) REFERENCES `company` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
