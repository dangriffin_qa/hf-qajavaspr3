package com.qa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import com.qa.beans.Animal;
import com.qa.beans.Owner;

@Configuration
public class SpringConfig {
	
	//Bean for the database connection
	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		
	}

	@Bean
	public DataSource dataSource() {
		
	}
	
	
	private Date getDate(String dateStr){
		try {
			return new SimpleDateFormat("dd-MM-yyyy").parse(dateStr);
		} catch (ParseException e) {
			return new Date();
		}
	}
	
	//beans for five animals
	@Bean
	public Animal alpha(){
		
	}
	
	@Bean
	public Animal beta(){
		
	}
	
	@Bean
	public Animal gamma(){
		
	}
	
	@Bean
	public Animal delta(){
		
	}
	
	@Bean
	public Animal epsilon(){
		
	}
	
	//beans for an owner
	@Bean 
	public Owner alice(){
		
	}
	
	@Bean 
	public Owner bob(){
		
	}
		
}
