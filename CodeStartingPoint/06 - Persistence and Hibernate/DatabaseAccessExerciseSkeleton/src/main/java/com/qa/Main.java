package com.qa;

import java.util.ArrayList;
import java.util.HashMap;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.qa.beans.Animal;
import com.qa.beans.Owner;

public class Main {
	public static void main(String[] args) {
		new Main();
	}

	public Main() {
		// get the spring context from annotations or xml
		
		// get the database connection bean from the context
		
		// get the beans from the context
		
		// save all the objects to the database
		
		//close the connection to the database
		
		
		// get the objects back out the database
		
		//get another session
		

		//create the query
		
		//close the database connection and the session factory
		
		// print them to System.out
		
	}
}
